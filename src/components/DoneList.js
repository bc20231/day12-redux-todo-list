import DoneItem from './DoneItem';
import { useSelector } from 'react-redux';

const DoneList = () => {
    const doneItems = useSelector((state) => state.todoList.items)
        .filter(item => item.done);

    return (
        <div className='done-list'>
            <h3>Done List</h3>
            {doneItems.map((item) => (
                <DoneItem key={item.id} item={item} />
            ))}
        </div>
    );
}

export default DoneList;