import { useDispatch } from "react-redux"
import { loadItems } from "../../app/todoListSlice";
import { createTodo, deleteTodo, getTodos, updateTodo } from "../../api/todos";

export const useTodos = () => {
    const dispatch = useDispatch();
    const loadTodos = () => {
        getTodos().then((response) => {
            dispatch(loadItems(response.data));
        });
    };

    const addTodo = async (text) => {
        await createTodo(text);
        loadTodos();
    }

    const editTodo = async (id) => {
        await updateTodo(id);
        loadTodos();
    }

    const removeTodo = async (id) => {
        await deleteTodo(id);
        loadTodos();
    }

    return {
        loadTodos,
        addTodo,
        editTodo,
        removeTodo
    }
};