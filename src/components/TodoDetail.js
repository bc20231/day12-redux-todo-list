import {useParams} from "react-router-dom";

const TodoDetail = () => {
    const {id} = useParams();

    return (
        <div>
            <h3>Todo Detail</h3>
            <p>{id}</p>
        </div>
    );
}

export default TodoDetail;