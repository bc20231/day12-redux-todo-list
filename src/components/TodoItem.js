import { useState } from "react";
import { useTodos } from "./hooks/useTodos";
import { EditOutlined } from '@ant-design/icons';
import { Modal, Input } from 'antd';

const TodoItem = ({item}) => {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [inputValue, setInputValue] = useState(item.text);
    const { editTodo, removeTodo } = useTodos();

    const handleToggle = () => {
        if (!isModalVisible) {
            editTodo({...item, done: !item.done});
        }
    };

    const handleDelete = (event) => {
        event.stopPropagation();
        if (window.confirm("Are you sure you want to delete this item?")) {
            removeTodo(item.id);
        }
    };

    const showEditBox = (event) => {
        event.stopPropagation();
        setIsModalVisible(true);
    };

    const handleEdit = (event) => {
        event.stopPropagation();
        editTodo({...item, text: inputValue});
        setInputValue(inputValue);
        setIsModalVisible(false);
    };

    const handleCancel = (event) => {
        event.stopPropagation();
        setInputValue(item.text);
        setIsModalVisible(false);
    };

    const handleInputChange = (event) => {
        event.stopPropagation();
        setInputValue(event.target.value);
    };

    return (
        <div className="todo-item" onClick={handleToggle}>
            <p className={item.done ? 'strike-through' : ''}>{item.text}</p>
            <span className="delete-icon" onClick={handleDelete}>x</span>
            <EditOutlined onClick={showEditBox}/>
            <Modal
                className="edit-box"
                title="Todo Item"
                visible={isModalVisible}
                onOk={handleEdit}
                onCancel={handleCancel}
                okButtonProps={{ disabled: !inputValue.trim() }}
            >
                <Input value={inputValue} onChange={handleInputChange} />
            </Modal>
        </div>
    )
}

export default TodoItem;