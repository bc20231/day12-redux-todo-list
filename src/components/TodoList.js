import TodoGroup from './TodoGroup';
import TodoGenerator from './TodoGenerator';
import './TodoList.css';
import { useEffect } from 'react';
import { useTodos } from './hooks/useTodos';

const TodoList = () => {
    const { loadTodos } = useTodos();

    useEffect(() => {
        loadTodos();
    }, []);

    return (
        <div>
            <h3>Todo List</h3>
            <TodoGroup />
            <TodoGenerator />
        </div>
    );
}

export default TodoList;