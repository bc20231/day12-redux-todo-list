import TodoItem from './TodoItem';
import { useSelector } from 'react-redux';

const TodoGroup = () => {
    const items = useSelector((state) => state.todoList.items);

    return (
        <div className='todo-group'>
            {items.map((item) => (
                <TodoItem key={item.id} item={item} />
            ))}
        </div>
    );
}

export default TodoGroup;