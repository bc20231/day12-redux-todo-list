import { Link } from 'react-router-dom';
import './DoneList.css';

const DoneItem = ({item}) => {
    return (
        <Link to={"/todos/" + item.id}>
            <div className="done-item">
                <p>{item.text}</p>
            </div>
        </Link>
    )
}

export default DoneItem;