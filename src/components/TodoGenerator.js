import { useState } from "react";
import { useTodos } from "./hooks/useTodos";
import { Button } from 'antd';

const TodoGenerator = () => {
    const { addTodo } = useTodos();
    const [inputValue, setInputValue] = useState('');

    const handleInputChange = (e) => {
        setInputValue(e.target.value);
    };

    const generateHandler = (event) => {
        event.preventDefault();
        const {value} = event.target[0];
        addTodo({text: value});
        setInputValue("");
    }

    return (
        <form onSubmit={generateHandler}>
            <input id="add-item" type="text" placeholder="input a new todo here..." value={inputValue} onChange={handleInputChange}/>
            <Button type="primary" htmlType="submit" disabled={!inputValue?.trim()}>add</Button>
        </form>
    )
}

export default TodoGenerator;