import axios from "axios";

const client = axios.create({
    baseURL: "https://6566e19a64fcff8d730f36bf.mockapi.io/"
})

export default client;