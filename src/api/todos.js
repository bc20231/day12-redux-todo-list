import client from "./client";

const getTodos = () => {
    return client.get("/todos");
};

const createTodo = (newItem) => {
    return client.post("/todos", newItem);
};

const updateTodo = (item) => {
    return client.put("/todos/" + item.id, item);
};

const deleteTodo = (id) => {
    return client.delete("/todos/" + id);
};

export {getTodos, createTodo, updateTodo, deleteTodo};