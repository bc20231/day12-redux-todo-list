import { Link } from 'react-router-dom';

const Navigation = () => {
    return (
        <nav>
            <ul>
                <li><Link to={"/about"}>About</Link></li>
                <li><Link to={"/"}>Todo List</Link></li>
                <li><Link to={"/done"}>Done List</Link></li>
            </ul>
        </nav>
    );
}

export default Navigation;