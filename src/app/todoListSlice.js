import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	items: []
}

const todoListSlice = createSlice({
	name: "todoList",
	initialState,
	reducers: {
        loadItems: (state, action) => {
            state.items = action.payload;
        }
	}
})

export const {loadItems} = todoListSlice.actions;
export default todoListSlice.reducer;